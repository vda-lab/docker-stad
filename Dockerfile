FROM ubuntu
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y python3 python3-pip python3-dev gcc
RUN pip3 install pystad

# FROM alpine:3.14
# RUN apk add --no-cache python3 py3-pip python3-dev gcc
# RUN pip3 install pystad
# RUN apk add --no-cache mysql-client
# ENTRYPOINT ["mysql"]